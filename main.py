import ircutil
import random
from datetime import datetime
from lxml.html import fromstring
import requests
import os
import re
#import time
#import ssl
#import socket

# IRC Config
bot = ircutil.Connection()
bot.server = "hell"
#bot.port = 6667
bot.nick = "rbot"
bot.hostname = "rbot"
bot.servername = "rbot"
bot.ident = "rbot"


random.seed(datetime.now().second)


@bot.trigger("WELCOME")
def autojoin(event):
    print("Bot is running")
    bot.join('#general')
    
@bot.trigger(lambda event: event.MSG and event.msg.startswith("$hi"))
def hi(event):
    bot.msg(event.chat, f'{event.nick}: what is your problem mate?')

@bot.trigger(lambda event: event.MSG and event.msg.startswith("$idot"))
def idot(event):
    bot.msg(event.chat, f"idot's anthem is being played in the radio")
    os.system("sh con.sh")    
        

@bot.trigger(lambda event: event.MSG and event.msg.startswith("$play"))
def play(event):
    if len(event.msg.split()) < 2:
        bot.msg(event.chat, f"song?")
    else:
        bot.msg(event.chat, f'Searching {event.msg.split()[1]}')
        os.system("rhythmbox-client --play-uri=~/Music/" + event.msg.split()[1])

@bot.trigger(lambda event: event.MSG and event.msg.startswith("$radio"))
def radio(event):
        response = requests.get("site")
        tree = fromstring(response.content)
        bot.msg(event.chat, "Currently playing: " + tree.xpath("/html/body/div[2]/div[2]/table/tbody/tr[9]/td[2]")[0].text)


@bot.trigger(lambda event: event.MSG and event.msg.startswith("$skip"))
def skip(event):
    bot.msg(event.chat, f"Skipping the song.")
    os.system("rhythmbox-client --next")
    

@bot.trigger(lambda event: event.MSG and event.msg.startswith("$previous"))                                                              
def previous(event):                                                                                                                                      
    bot.msg(event.chat, f"Playing the previous song")                                                                                                      
    os.system("rhythmbox-client --previous")
                                          

@bot.trigger(lambda event: event.MSG and event.msg.startswith("$request"))     
def request(event):                                                                                   
    bot.msg(event.chat, f"Downloading the song")                                                       

    os.system("rm request.mp3 ; yt-dlp --extract-audio --audio-format mp3 " + event.msg.split()[1] + " --output request.mp3")
    
    #bot.msg(event.chat, f"Playing the song " + os.system("yt-dlp --get-filename " + event.msg.split()[1]))

    os.system("rhythmbox-client --play-uri=request.mp3")

bot.connect()
